package com.epam.model.generics;

import java.util.Collection;
import java.util.List;

public class Container<T>{
    public <E> void test(Collection<E> ts){
        System.out.println("generic");
    }
    public void test(List<Integer>integers){
        for (int i = 0; i <integers.size() ; i++) {
            System.out.println(integers.get(i));
        }
    }
}
