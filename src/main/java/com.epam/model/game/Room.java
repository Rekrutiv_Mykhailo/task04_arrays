package com.epam.model.game;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Room {
    private Player player;
    private Door[] doors;

    public Room() {
    }

    public Room(Player player, Door[] door) {
        this.player = player;
        this.doors = door;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Door[] getDoors() {
        return doors;
    }

    public void setDoors(Door[] doors) {
        this.doors = doors;
    }

    public Room createRoom() {
        Random random = new Random();
        Random random1 = new Random(95);
        Random random2 = new Random(70);
        Player player = new Player("Mykhailo");
        int numb;
        Door<Character>[] characterDoor = new Door[10];
        for (int i = 0; i < characterDoor.length; i++) {
            numb = random.nextInt(2);
            if (numb == 0) {
                Monster monster = new Monster(-1 * (random.nextInt(95) + 5));
                Door door = new Door();
                door.setT(monster);
                characterDoor[i] = door;

            } else {
                Artefact artefact = new Artefact(random.nextInt(70) + 10);
                Door door = new Door();
                door.setT(artefact);
                characterDoor[i] = door;

            }
        }
        Room room = new Room(player, characterDoor);
        return room;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return Objects.equals(player, room.player) &&
                Arrays.equals(doors, room.doors);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(player);
        result = 31 * result + Arrays.hashCode(doors);
        return result;
    }

    @Override
    public String
    toString() {
        return "Room{" +
                "player=" + player +
                ", doors=" + Arrays.toString(doors) +
                '}';
    }
}
