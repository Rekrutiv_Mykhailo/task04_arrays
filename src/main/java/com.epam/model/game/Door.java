package com.epam.model.game;

public class Door<T> {
    T t;

    public Door() {
    }

    public Door(T t) {
        this.t = t;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    @Override
    public String toString() {
        return "Door{" +
                "t=" + t +
                '}';
    }


}
