package com.epam.model.game;

public class Player extends Character {
    private String name;

    public Player(String name) {
        super(25);
        this.name = name;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                '}';
    }
}
