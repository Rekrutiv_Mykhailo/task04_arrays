package com.epam.model.game;

public class Character {
    private int helth;
    private String characterType;

    public Character() {
    }

    public Character(int helth) {
        this.helth = helth;
    }

    public Character(int helth, String characterType) {
        this.helth = helth;
        this.characterType = characterType;
    }

    public int getHelth() {
        return helth;
    }

    public void setHelth(int helth) {
        this.helth = helth;
    }

    @Override
    public String toString() {
        return "Character{" +
                "helth=" + helth +
                ", characterType='" + characterType + '\'' +
                '}';
    }

    public String getCharacterType() {
        return characterType;
    }

    public void setCharacterType(String characterType) {
        this.characterType = characterType;
    }
}
