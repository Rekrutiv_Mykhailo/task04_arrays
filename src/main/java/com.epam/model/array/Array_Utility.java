package com.epam.model.array;

import java.util.Arrays;

public class Array_Utility {
    public static void main(String[] args) {
        int[] arr = {1, 3, 5, 7, 9, 11, 12};
        int[] arr1 = {1, 2, 3, 4, 5, 6, 7, 11, 8};
        arr = new Array_Utility().findAnotherElements(arr1, arr);

    }

    private int[] decreaseArrayOnNumb(int[] arr, int index) {
        int[] mass = new int[arr.length - index];
        for (int i = 0; i < mass.length; i++) {
            mass[i] = arr[i];
        }
        return mass;
    }

    public int[] deleteSameTwoGradualMassiveElements(int[] arr) {
        int[] massive = new int[arr.length];
        int j = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] != arr[i + 1]) {
                massive[j] = arr[i];
                j++;
            }

        }
        massive[j] = arr[arr.length - 1];
        massive = decreaseArrayOnNumb(massive, arr.length - 1 - j);
        return massive;
    }

    public int[] deleteSameMassiveElements(int[] arr) {
        Arrays.sort(arr);
        return deleteSameTwoGradualMassiveElements(arr);
    }

    public int[] findSameElements(int[] arr1, int[] arr2) {
        arr1 = deleteSameMassiveElements(arr1);
        arr2 = deleteSameMassiveElements(arr2);
        int l = 0;
        int[] outputArray = new int[arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    outputArray[l] = arr1[i];
                    l++;
                }
            }
        }
        outputArray = decreaseArrayOnNumb(outputArray, outputArray.length - l);

        return outputArray;
    }

    public int[] findAnotherElements(int[] arr1, int[] arr2) {
        arr1 = deleteSameMassiveElements(arr1);
        arr2 = deleteSameMassiveElements(arr2);
        boolean isSame = false;
        int[] outputArray = new int[arr1.length + arr2.length];
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j <arr2.length ; j++) {
                if (arr1[i]==arr2[j]){
                    isSame= true;
                }
            }
            if (!isSame){
                System.out.println(arr1[i]);
            }
            isSame = false;
        }

        return outputArray;
    }
}
