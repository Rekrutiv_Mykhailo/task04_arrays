package com.epam.view;

import com.epam.controller.Array_Controller;
import com.epam.controller.GameController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainView {
    private static final String string = "Door1      " + "Door2     " + "Door3     "
            + "Door4     " + "Door5    " + "Door6    " + "Door7    " + "Door8     " + "Door9    " + "Door10     ";
    private final static Logger LOGGER = LogManager.getLogger(MainView.class);

    public MainView() {
        LOGGER.info(new Array_Controller().loadData());
        LOGGER.info(string);
        LOGGER.info(new GameController().generateDoorInfo());
    }




}
