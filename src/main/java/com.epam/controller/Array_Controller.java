package com.epam.controller;

import com.epam.model.array.Array_Utility;

public class Array_Controller {
    public static void main(String[] args) {

    }

    public String loadData() {
        int[] arr = {1, 3, 5, 7, 9, 11, 12};
        int[] arr1 = {1, 2, 3, 4, 5, 6, 7, 11, 8};
        String firstTask1 = "First Task 1: ";
        String secondTask = "Second Task: ";
        String thirdTask = "Third Task: ";
        String view = "";
        Array_Utility array_utility = new Array_Utility();
        Array_Controller array_controller = new Array_Controller();
        int[] elements = array_utility.findSameElements(arr, arr1);
        firstTask1 = array_controller.createTaskLine(firstTask1, elements);
        elements = array_utility.deleteSameMassiveElements(arr);
        secondTask = array_controller.createTaskLine(secondTask, elements);
        elements = array_utility.deleteSameTwoGradualMassiveElements(arr);
        thirdTask = array_controller.createTaskLine(thirdTask, elements);
        view += firstTask1  + "\n"+secondTask + "\n"+ thirdTask + "\n";

        return view;
    }

    private String createTaskLine(String line, int[] numbs) {
        for (int i = 0; i < numbs.length; i++) {
            line += numbs[i] + " ";
        }
        return line;
    }
}
