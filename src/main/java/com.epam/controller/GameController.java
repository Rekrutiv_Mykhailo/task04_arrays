package com.epam.controller;

import com.epam.model.game.Artefact;
import com.epam.model.game.Monster;
import com.epam.model.game.Room;

public class GameController {
    public Room loadRoom(){
        return new Room().createRoom() ;
    }
    public String generateDoorInfo() {
        String info = "";
        String infoHealth = "";
        Room room = new GameController().loadRoom();
        int countDeath = 0;
        for (int i = 0; i < room.getDoors().length; i++) {

            if (room.getDoors()[i].getT().getClass().getSimpleName().equals("Monster")) {
                Monster character = (Monster) room.getDoors()[i].getT();
                info += character.getCharacterType() + "  ";
                infoHealth += character.getHelth() + "       ";
                if (room.getPlayer().getHelth() < -1 * character.getHelth()) {
                    countDeath++;
                }
            } else if (room.getDoors()[i].getT().getClass().getSimpleName().equals("Artefact")) {
                Artefact character = (Artefact) room.getDoors()[i].getT();
                info += character.getCharacterType() + "  ";
                infoHealth += character.getHelth() + "        ";
            }
        }

        String view = info + "\n" + infoHealth + "\n" + countDeath + " doors is fatal for you";

        return view;
    }
}
